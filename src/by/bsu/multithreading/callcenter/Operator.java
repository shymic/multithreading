package by.bsu.multithreading.callcenter;

import by.bsu.multithreading.exception.LogcalException;

import java.util.concurrent.TimeUnit;

/**
 * Created by Andrey on 15.03.2015.
 */
public class Operator {

    private final int ID;

    public Operator() {
        ID = IdGenerator.generate();
    }

    public int getID() {
        return ID;
    }

    public void operate(int serviceTime) throws LogcalException {
        try {
            TimeUnit.MILLISECONDS.sleep(serviceTime);
        } catch (InterruptedException e) {
            throw new LogcalException(e);
        }
    }
}