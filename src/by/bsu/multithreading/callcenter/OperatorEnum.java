package by.bsu.multithreading.callcenter;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Andrey on 17.03.2015.
 */
public enum OperatorEnum {
    INSTANCE( new ConcurrentLinkedQueue<Operator>() {
        {
            this.add(new Operator());
            this.add(new Operator());
            this.add(new Operator());
            this.add(new Operator());
            this.add(new Operator());
        }
    });

    private ConcurrentLinkedQueue<Operator> operators;

    OperatorEnum(ConcurrentLinkedQueue<Operator> values) {
        operators = values;
    }

    public ConcurrentLinkedQueue<Operator> getOperators() {
        return operators;
    }

}
