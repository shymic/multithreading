package by.bsu.multithreading.callcenter;

/**
 * Created by Andrey on 18.03.2015.
 */
public class IdGenerator {
    private static int firstID;
    static {
        firstID = 100;
    }
    public IdGenerator() {
    }

    public static int getFirstID() {
        return firstID;
    }

    public static void setFirstID(int firstID) {
        IdGenerator.firstID = firstID;
    }

    public static int generate(){
        return firstID++;
    }
}
