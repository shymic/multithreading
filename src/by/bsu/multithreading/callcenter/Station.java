package by.bsu.multithreading.callcenter;

/**
 * Created by Andrey on 15.03.2015.
 */
import by.bsu.multithreading.exception.ResourсeException;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Station {
    public final static int SIZE = 5;
    private final Semaphore SEMAPHORE = new Semaphore(SIZE, true);
    private OperatorEnum operators;

    private Station() {}

    private static class StationHolder {
        public static Station station = new Station();
    }

    public static Station getStation() {
        return StationHolder.station;
    }

    public Operator getResource(long maxWaitMillis) throws ResourсeException {
        try {
            if(SEMAPHORE.tryAcquire(maxWaitMillis, TimeUnit.MILLISECONDS)) {
                Operator res = operators.INSTANCE.getOperators().poll();
                return res;
            }
            else{
                TimeUnit.MILLISECONDS.sleep(10);
                return getResource(maxWaitMillis);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        throw new ResourсeException("can't get resours");
    }

    public void returnResource(Operator res) {
        operators.INSTANCE.getOperators().add(res);
        SEMAPHORE.release();
    }
}