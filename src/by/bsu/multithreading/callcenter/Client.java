package by.bsu.multithreading.callcenter;

import by.bsu.multithreading.exception.LogcalException;
import by.bsu.multithreading.exception.ResourсeException;
import org.apache.log4j.Logger;

/**
 * Created by Andrey on 15.03.2015.
 */
public class Client extends Thread {
    private static final Logger LOG = Logger.getLogger(Client.class);
    private int serviceTime;
    private int waitTime;
    private boolean wantSpeack = true;
    private Station pool;

    public Client (Station pool, int serviceTime, int maxWaitTime) {
        this.serviceTime = serviceTime;
        this.pool = pool;
        this.waitTime = maxWaitTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public int getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }

    public void run() {
        Operator operator = null;
        try {
                operator = pool.getResource(waitTime);
                LOG.info("Client # " + this.getId() + " serviced by operator # " + operator.getID());
                operator.operate(serviceTime);

        } catch (ResourсeException | LogcalException e) {
            LOG.info("Client # " + this.getId() + " lost -> " + e.getMessage());
        } finally {
            if (operator != null) {
                LOG.info("Client #" + this.getId() + " : " + operator.getID() + " operator released");
                pool.returnResource(operator);
            }
        }
    }

}