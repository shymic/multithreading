package by.bsu.multithreading.exception;

/**
 * Created by Andrey on 16.03.2015.
 */
public class LogcalException extends Exception {
    public LogcalException() {
    }

    public LogcalException(String message) {
        super(message);
    }

    public LogcalException(String message, Throwable cause) {
        super(message, cause);
    }

    public LogcalException(Throwable cause) {
        super(cause);
    }

    public LogcalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
