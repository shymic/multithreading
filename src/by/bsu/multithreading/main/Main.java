package by.bsu.multithreading.main;

/**
 * Created by Andrey on 15.03.2015.
 */
import by.bsu.multithreading.callcenter.Client;
import by.bsu.multithreading.callcenter.Station;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.Random;


public class Main {
    static {
        new DOMConfigurator().doConfigure("resources\\log4j.xml", LogManager.getLoggerRepository());
    }

    public static void main(String[] args) {
        Station station = Station.getStation();
        for (int i = 0; i < 10; i++) {
           new Client(station, new Random().nextInt(400),new Random().nextInt(400)).start();
        }
    }
}